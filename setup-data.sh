#!/bin/bash -e

# Get the archive, check its hash, and unzip it
wget "http://mikexcohen.com/book/AnalyzingNeuralTimeSeriesData_MatlabCode.zip"
echo "00d1e001de46a04bb21b68be0874cb6e6aff29ae  AnalyzingNeuralTimeSeriesData_MatlabCode.zip" | sha1sum -c
unzip -d lectures-material AnalyzingNeuralTimeSeriesData_MatlabCode.zip

# Copy what we need
mkdir -p data
# Fill this as we go through the lectures, e.g.:
cp lectures-material/code/sampleEEGdata.mat data/

# Clean up
rm -r lectures-material AnalyzingNeuralTimeSeriesData_MatlabCode.zip
