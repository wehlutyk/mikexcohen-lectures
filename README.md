Mike X Cohen's Time-Frequency data analysis lectures
====================================================

Following the videos from http://mikexcohen.com/lectures.html

Setup
-----

First get the data by running the following short script:

```
./setup-data.sh
```

Then, we use [virtualenv](https://virtualenv.pypa.io/en/stable/) to manage Python environments. If your shell is bash (which is very likely, but you can check by running `echo $SHELL` in a terminal), using [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/stable/) to manage your environments will make your life easier.

I use the [fish](https://fishshell.com/) shell with its equally excellent [virtualfish](https://virtualfish.readthedocs.io/en/stable/install.html) extension.

Here is the command to set up the environment using `virtualenvwrapper`:

```
mkvirtualenv -p python3.7 mikexcohen-lectures  # or `vf new ...` with fish
workon mikexcohen-lectures                     # or `vf activate ...` with fish
pip install -r requirements.lock.txt
```

Lectures
--------

This is the list of lectures completed so far:

* [EEG data and indexing in Matlab](http://mikexcohen.com/lecturelets/indexing/indexing.html): [`notebooks/01-indexing.ipynb`](https://gitlab.com/wehlutyk/mikexcohen-lectures/blob/master/notebooks/01-indexing.ipynb)
* [Overview of time-domain analyses](https://mikexcohen.com/lecturelets/timedomain/timedomain.html): [`notebooks/02-timedomain.ipynb`](https://gitlab.com/wehlutyk/mikexcohen-lectures/blob/master/notebooks/02-timedomain.ipynb)
* [Topographical plots](https://mikexcohen.com/lecturelets/topoplots/topoplots.html): ['notebooks/03-topoplots.ipynb'](https://gitlab.com/wehlutyk/mikexcohen-lectures/blob/master/notebooks/03-topoplots.ipynb)

TODO
----

* Try out [cufflinks+plotly](https://towardsdatascience.com/the-next-level-of-data-visualization-in-python-dd6e99039d5e) and [interactive controls](https://towardsdatascience.com/interactive-controls-for-jupyter-notebooks-f5c94829aee6)
