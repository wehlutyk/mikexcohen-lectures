"""Utility functions for working with EEG data."""


import numpy as np
import scipy.io


def closest_idx(array, value):
    """Find the index at which `array` is closest to `value`;
    if `array` is multidimensional, return a tuple."""
    amin_flat = np.argmin(np.abs(array - value))
    if len(array.shape) > 1:
        return np.unravel_index(amin_flat, array.shape)
    return amin_flat


def istring_idx(str_array, string):
    """Find the index of `string` in `str_array` (the match is
    case-insensitive); raise an error if no match is found or if
    several matches are found."""
    match_idx = np.where(np.char.lower(str_array) == string.lower())
    if len(match_idx[0]) == 0:
        raise ValueError("String '{}' not found".format(string))
    if len(match_idx[0]) > 1:
        raise ValueError("String '{}' found several times".format(string))
    if len(str_array.shape) > 1:
        return tuple(idx[0] for idx in match_idx)
    return match_idx[0][0]


def import_eeg_mat(filepath):
    """Load a Matlab eeg file and return structured contents in a namespace."""
    contents = scipy.io.loadmat(filepath, struct_as_record=False)
    eeg = contents['EEG'].item()

    # Our reconstructed (y, x) locations from the polar coordinates
    chanlocsYX = np.array([pol2cart(loc.radius.item(),
                                    loc.theta.item() * np.pi / 180)
                           for loc in eeg.chanlocs.flat])[:, ::-1]
    # The labels of the electrodes (same length as `chanlocs`)
    chanlabels = np.array([loc.labels.item() for loc in eeg.chanlocs.flat])

    return Bunch({
        'chanlocsYX': chanlocsYX,
        'chanlabels': chanlabels,
        # The array of timestamps, in milliseconds,
        # with time 0 being stimulus onset
        'times': np.squeeze(eeg.times),
        # The actual measurments
        'data': eeg.data,
        # Number of trials
        'trials': np.squeeze(eeg.trials).item()
    })


def cart2pol(x, y):
    """Convert cartesian coordinates to polar."""
    radius = np.sqrt(x**2 + y**2)
    theta = np.arctan2(y, x)
    return radius, theta


def pol2cart(radius, theta):
    """Convert polar coordinates to cartesian."""
    x = radius * np.cos(theta)
    y = radius * np.sin(theta)
    return x, y


class Bunch:

    """Turn a dict into a namespace, accessing fields as `d.field`
    instead of `d['field']`."""

    def __init__(self, fields):
        self.__dict__.update(fields)
